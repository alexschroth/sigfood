# <img src="Images/Sigfood_iOS_with_background.png " width="80px"/> Sigfood

iOS Speiseplan App für die Mensa am Südegelände der FAU. Nutzt [sigfood.de](https://sigfood.de) als Datenquelle - daher der Name. 

[![Available on the App Store](App_Store_Badge.png?raw=true)](https://itunes.apple.com/de/app/sigfood.de-speiseplan-sudmensa/id1063704585?mt=8)