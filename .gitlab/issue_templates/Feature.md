## Zusammenfassung
(beschreibe bitte kurz, worum es geht)


## Beschreibung
(beschreibe die Idee, die Funktion oder Verbesserung so ausführlich wie möglich)

## Vorteile
(warum ist das Feature wichtig?)

## Beispiel
(hast du ein Beispiel?)

## Voraussetzungen
(welche Voraussetzungen sind für die Umsetzung notwendig?)

## Links / Referenzen


/label ~feature ~review ~needs-investigation