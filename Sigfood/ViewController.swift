//
//  ViewController.swift
//  Sigfood
//
//  Created by Kett, Oliver on 30.09.14.
//  Copyright (c) 2014 Kett, Oliver. All rights reserved.
//

// todo: 
// * upload fotos and write comments

import UIKit
import CoreData
import JGProgressHUD
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, XMLParserDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var buttonDate: UIButton!
    
    // pull to refresh
    var refreshControl = UIRefreshControl()
    
    // now
    var date = Date()
    let oneDayinSeconds: Double = 24 * 60 * 60
    // store users chosen locale for date formatting in header
    let calendar = Calendar.current
    let formatter = DateFormatter()
    
    let context = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext // CoreData
    var data = [Menu]()
    var imageID: Int = 0
    
    // initialize loading animation bevore first use
    var loadingAnimation = JGProgressHUD(style: JGProgressHUDStyle.dark)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableView.automaticDimension
        
        self.refreshControl.tintColor = UIColor(red: 0.016, green: 0.710, blue: 0.788, alpha: 1.00)
        self.refreshControl.addTarget(self, action: #selector(ViewController.forceUpdateDatabase), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
        
        //setup date format with users
        self.formatter.locale = Locale.current
        self.formatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "E ddMMyy", options: 0, locale: Locale.current)
        
        cleanDatabaseForThreshold(7)
        updateUITableView(force: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showPrevious(_ sender: AnyObject) {
        self.date = self.date.addingTimeInterval(-oneDayinSeconds)
        updateUITableView(force: false)
    }
    @IBAction func buttonSetDateToToday(_ sender: UIButton, forEvent event: UIEvent) {
        self.date = Date()
        updateUITableView(force: false)
    }
    
    @IBAction func showNext(_ sender: AnyObject) {
        self.date = self.date.addingTimeInterval(oneDayinSeconds)
        updateUITableView(force: false)
    }
    
    // MARK: TableView delegate
    // UITableViewDataSource methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // we display one row even if there are no dishes
        if self.data.count == 0 {
            return 1
        } else {
            return data.count
        }
    }
    
    // here be dragons
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // no dishes found
        if self.data == [] {
            let cell: UITableViewCell
            let weekday = (self.calendar as NSCalendar).component(.weekday, from: self.date)
            if (weekday == 1) || (weekday == 7) {
                cell = tableView.dequeueReusableCell(withIdentifier: "weekendCell")!
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell")!
            }
            return cell
        }
        
        // if we have dishes
        let cell = tableView.dequeueReusableCell(withIdentifier: "sigfoodCell") as! SigfoodTableViewCell
        let row = self.data[indexPath.row]
        
        cell.mainCourseLabel.text = row.mainCourse
        cell.priceGuest.text = row.priceGuest
        cell.priceStud.text = row.priceStudent
        cell.priceStaff.text = row.priceEmployee
        
        let emojiGenerator = EmojiImageGenerator()
        if row.veggie == true {
            cell.veggie.image = emojiGenerator.imageWithEmoji(EmojiImageGenerator.emoji.apple, size: 64.0)
        } else {
            cell.veggie.image = UIImage()
        }
        if row.beef == true {
            cell.beef.image = emojiGenerator.imageWithEmoji(EmojiImageGenerator.emoji.cow, size: 64.0)
        } else {
            cell.beef.image = UIImage()
        }
        if row.withoutPork == true {
            cell.pork.image = emojiGenerator.prohibitedImage(EmojiImageGenerator.emoji.pig, size: 64.0)
        } else {
            cell.pork.image = UIImage()
        }
        
        if row.image != nil {
            cell.foodImage.image = UIImage(data: row.image! as Data)
        } else {
            cell.foodImage.image = emojiGenerator.imageWithEmoji(EmojiImageGenerator.emoji.white_questionmark, size: 512.0)
        }
        cell.foodImage.clipsToBounds = true
        cell.foodImage.contentMode = .scaleAspectFill
        
        if row.score?.doubleValue > 0.5 {
            cell.star1.image = emojiGenerator.imageWithEmoji(EmojiImageGenerator.emoji.star, size: 64.0)
        } else {
            cell.star1.image = emojiGenerator.imageWithEmoji(EmojiImageGenerator.emoji.white_star, size: 64.0)
        }
        if row.score?.doubleValue > 1.5 {
            cell.star2.image = emojiGenerator.imageWithEmoji(EmojiImageGenerator.emoji.star, size: 64.0)
        } else {
            cell.star2.image = emojiGenerator.imageWithEmoji(EmojiImageGenerator.emoji.white_star, size: 64.0)
        }
        if row.score?.doubleValue > 2.5 {
            cell.star3.image = emojiGenerator.imageWithEmoji(EmojiImageGenerator.emoji.star, size: 64.0)
        } else {
            cell.star3.image = emojiGenerator.imageWithEmoji(EmojiImageGenerator.emoji.white_star, size: 64.0)
        }
        if row.score?.doubleValue > 3.5 {
            cell.star4.image = emojiGenerator.imageWithEmoji(EmojiImageGenerator.emoji.star, size: 64.0)
        } else {
            cell.star4.image = emojiGenerator.imageWithEmoji(EmojiImageGenerator.emoji.white_star, size: 64.0)
        }
        if row.score?.doubleValue > 4.5 {
            cell.star5.image = emojiGenerator.imageWithEmoji(EmojiImageGenerator.emoji.star, size: 64.0)
        } else {
            cell.star5.image = emojiGenerator.imageWithEmoji(EmojiImageGenerator.emoji.white_star, size: 64.0)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // if we display only one single cell
        if self.data.count == 0 {
            self.tableView.separatorStyle = .none
            return self.view.bounds.width
        } else {
            self.tableView.separatorStyle = .singleLine
            // fixed size for now ...
            // try this: https://www.youtube.com/watch?v=rgazh3vixQw
            return 120
            //return tableView.bounds.size.height / CGFloat(self.data.count)
        }
    }
    
    // MARK: Image loading
    @IBAction func showImage(_ sender: AnyObject) {
        let buttonPos = sender.convert(CGPoint.zero, to: self.tableView)
        let row = self.tableView.indexPathForRow(at: buttonPos)?.row
        let imageID = self.data[row!].imageID?.intValue
        if imageID > 0 {
            // there should be a better way to say the SigfoodImageViewController what the imageID is ...
            performSegue(withIdentifier: "picture", sender: imageID)
        }
    }
    
    // MARK: Segue
    // load comments on tap
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "comments" {
            if let row = tableView.indexPath(for: sender as! UITableViewCell)?.row {
                // we give the CommentVC the current menu and he fetches the comments for it itself
                (segue.destination as! CommentViewController).menu = data[row]
            }
        } else if segue.identifier == "picture" {
            // there should be a better way to say the SigfoodImageViewController what the imageID is ...
            let sender = sender as! Int
            if sender > 0 {
                (segue.destination as! SigfoodImageViewController).imageID = sender
            }
            self.imageID = 0
        }
    }
    
    // MARK: fetch and update Database
    
    // there should be a cleaner way to do this
    @objc func forceUpdateDatabase() {
        updateUITableView(force: true)
        self.refreshControl.endRefreshing()
    }
    
    func updateUITableView(force: Bool) {
        // set force to true to update every time
        OperationQueue().addOperation {
            self.data = self.fetchDatabase()
            if force {
                Log("Update forced: delete everything!")
                for dish in self.data {
                    self.context.delete(dish)
                }
                self.data.removeAll()
            }
            if self.data.count == 0 {
                self.updateDatabase()
                self.data = self.fetchDatabase()
            }
            
            // back on the main Queue we update the header and load the Cells
            OperationQueue.main.addOperation {
                self.buttonDate.setTitle(self.formatter.string(from: self.date), for: UIControl.State())
                // reload tableView
                self.tableView.reloadData()
            }
        }
    }
    
    func fetchDatabase() -> [Menu] {
        let result: [Menu]
        do {
            let calendar = Calendar.current
            
            let today = (calendar as NSCalendar).components([.year, .month, .day], from: self.date)
            let normalizedToday = calendar.date(from: today)
            
            let tomorrow = (calendar as NSCalendar).components([.year, .month, .day], from: self.date.addingTimeInterval(self.oneDayinSeconds))
            let normalizedTomorrow = calendar.date(from: tomorrow)
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Menu")
            fetchRequest.predicate = NSPredicate(format: "(date > %@) AND (date < %@)", normalizedToday! as CVarArg, normalizedTomorrow! as CVarArg)
            fetchRequest.sortDescriptors = [NSSortDescriptor(key: "mainCourse", ascending: true)]
            
            result = try self.context.fetch(fetchRequest) as! [Menu]
            return result
        } catch {
            Log("Error loading Data from DB: \(error)")
            return []
        }
    }
    
    func updateDatabase() {
        // we are going to fetch data, so we give some feedback
        OperationQueue.main.addOperation() {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            //SwiftSpinner.show("loading")
            self.loadingAnimation!.show(in: self.view)
        }
        
        let year = (Calendar.current as NSCalendar).component(.year, from: self.date).description
        var month = (Calendar.current as NSCalendar).component(.month, from: self.date).description as NSString
        if month.doubleValue < 10 {
            month = "0\(month)" as NSString
        }
        var day = (Calendar.current as NSCalendar).component(.day, from: self.date).description as NSString
        if day.doubleValue < 10 {
            day = "0\(day)" as NSString
        }
        
        let url = URL(string: "https://www.sigfood.de/?do=api.gettagesplan&datum=\(year)-\(month)-\(day)")!
        Log("url: \(url)")
        guard let data = try? Data(contentsOf: url) else {
            OperationQueue.main.addOperation() {
                // SwiftSpinner.hide()
                self.loadingAnimation!.dismiss(animated: true)
                let alert = UIAlertController(
                    title: NSLocalizedString("error.httpfetch.title", comment: "There was an error fetching data from sigfood.de (title)"),
                    message: NSLocalizedString("error.httpfetch.message", comment: "There was an error fetching data from sigfood.de (message)"),
                    preferredStyle: .alert
                )
                alert.addAction(UIAlertAction(title: NSLocalizedString("error.httpfetch.button", comment: "There was an error fetching data from sigfood.de (Confirm Button)"), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            return
        }
        
        let mensaessen = SWXMLHash.parse(data)["Mensa"]["Tagesmenue"]["Mensaessen"]
        
        for index in 0..<mensaessen.all.count {
            // need a better word than "row"
            let dish = NSEntityDescription.insertNewObject(forEntityName: "Menu", into: self.context) as! Menu
            dish.date = self.date
            
            // and it's content
            let mainCourse = mensaessen[index]["hauptgericht"]["bezeichnung"].element?.text.htmlDecodedString() ?? ""
            let garnish = mensaessen[index]["beilage"]["bezeichnung"].element?.text.htmlDecodedString() ?? ""
            if garnish != "" {
                if mainCourse.contains(" mit ") {
                    dish.mainCourse =  "\(mainCourse) und \(garnish)"
                } else {
                    dish.mainCourse =  "\(mainCourse) mit \(garnish)"
                }
            } else {
                dish.mainCourse = mainCourse
            }
            
            // veggie, beef, without pork
            
            //if mensaessen[index].element?.attributes["vegetarisch"] == "true" {
            if mensaessen[index].element?.allAttributes["vegetarisch"]?.text == "true" {
                dish.veggie = true
            } else {
                dish.veggie = false
            }
            if mensaessen[index].element?.allAttributes["rind"]?.text == "true" {
                dish.beef = true
            } else {
                dish.beef = false
            }
            if mensaessen[index].element?.allAttributes["moslem"]?.text == "true" {
                dish.withoutPork = true
            } else {
                dish.withoutPork = false
            }
            
            // score
            if let averageScore = mensaessen[index]["hauptgericht"]["bewertung"]["schnitt"].element?.text {
                dish.score = NSString(string: averageScore).doubleValue as NSNumber
            } else {
                dish.score = 0
            }

            // function to convert the price string
            func formatCurrency(value: Double) -> String {
                let formatter = NumberFormatter()
                formatter.numberStyle = .currency
                formatter.locale = Locale(identifier: "de_DE")
                let result = formatter.string(from: value as NSNumber)
                return result!
            }

            // students
            if let priceStudent = mensaessen[index]["hauptgericht"]["preisstud"].element?.text {
                dish.priceStudent = formatCurrency(value: NSString(string: priceStudent).doubleValue/100)
            }

            // employees
            if let priceEmployee = mensaessen[index]["hauptgericht"]["preisbed"].element?.text {
                dish.priceEmployee = formatCurrency(value: NSString(string: priceEmployee).doubleValue/100)
            }
            // guests
            if let priceGuest = mensaessen[index]["hauptgericht"]["preisgast"].element?.text {
                dish.priceGuest = formatCurrency(value: NSString(string: priceGuest).doubleValue/100)
            }

            
            // check if we have food pictures
            if mensaessen[index]["hauptgericht"]["bild"].all.count > 0 {
                var pictures = [Int]()
                for picture in mensaessen[index]["hauptgericht"]["bild"].all {
                    // check if Id is an Int
                    if let id = Int((picture.element?.allAttributes["id"]?.text)!) {
                        pictures.append(id)
                    }
                }
             
            // assign picture and overwrite default one here ...
                if pictures.count >= 1 {
                    let random_id = Int(arc4random_uniform(UInt32(pictures.count)))
                    dish.imageID = pictures[random_id] as NSNumber
                    let url = URL(string: "https://www.sigfood.de/?do=getimage&bildid=\(pictures[random_id])&width=256")
                    if let image = try? Data(contentsOf: url!) {
                        dish.image = image
                    } else {
                        Log("download failed: \(String(describing: url))")
                    }
                }
             }


            // check for comments
            if mensaessen[index]["hauptgericht"]["kommentar"].all.count > 0 {
                for comment in mensaessen[index]["hauptgericht"]["kommentar"].all {
                    guard let text = comment["text"].element?.text else { continue }
                    guard let nick = comment["nick"].element?.text else { continue }
                    let timestamp = NSString(string: (comment["timestamp"].element?.text)!).doubleValue as TimeInterval
                    let newComment = NSEntityDescription.insertNewObject(forEntityName: "Comment", into: self.context) as! Comment
                    newComment.text = text.htmlDecodedString()
                    newComment.nickname = nick.htmlDecodedString()
                    newComment.timestamp = timestamp as NSNumber
                    // assign Comment to Menu
                    // menuRef is Type Menu, commentRef is Type NSSet(Comment)
                    // so we can only assign one-way
                    newComment.menuRef = dish
                }
            }
        }
        
        do {
            try self.context.save()
        } catch {
            Log("Error saving: \(error)")
        }
        
        // disable loading animation
        OperationQueue.main.addOperation() {
            // SwiftSpinner.hide()
            self.loadingAnimation!.dismiss(animated: true)
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    func cleanDatabaseForThreshold(_ threshold: Double) {
        Log("threshold is \(String(threshold))")
        let thresholdInSeconds = threshold * self.oneDayinSeconds
        let calendar = Calendar.current
        
        let threshold = (calendar as NSCalendar).components([.year, .month, .day], from: self.date.addingTimeInterval(-thresholdInSeconds))
        let normalizedThreshold = calendar.date(from: threshold)
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Menu")
        fetchRequest.predicate = NSPredicate(format: "date < %@", normalizedThreshold! as CVarArg)
        
        do {
            let result = try self.context.fetch(fetchRequest) as! [Menu]
            result.forEach({ dish in
                self.context.delete(dish)
            })
            try self.context.save()
        } catch {
            Log("Error: \(error)")
        }
    }
}
