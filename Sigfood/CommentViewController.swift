//
//  CommentViewController.swift
//  Sigfood
//
//  Created by Kett, Oliver on 16.11.15.
//  Copyright © 2015 Kett, Oliver. All rights reserved.
//

import UIKit
import CoreData

class CommentViewController: UITableViewController {
    
    var menu: Menu?
    var data = [Comment]()
    let context = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableView.automaticDimension
        
        self.title = menu?.mainCourse
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Comment")
        fetchRequest.predicate = NSPredicate(format: "menuRef = %@", menu!)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "timestamp", ascending: false)]
        
        do {
            data = try context.fetch(fetchRequest) as! [Comment]
        } catch {
            Log("Error fetching Comments: \(error)")
        }
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.data.count == 0 {
            return 1
        } else {
            return self.data.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "comment")!
        
        // we write at least one comment
        if self.data.count == 0 {
            cell.textLabel!.text = "Noch kein Kommentar vorhanden!"
            cell.detailTextLabel!.text = String()
            return cell
        } else {
            let comment = data[indexPath.row]
            
            let formatter = DateFormatter()
            formatter.setLocalizedDateFormatFromTemplate("yMMMMd")
            let date = formatter.string(from: Date(timeIntervalSince1970: Double(truncating: comment.timestamp!)))
            
            cell.textLabel!.text = comment.text!
            cell.detailTextLabel!.text = "\(comment.nickname!) am \(date)"

            return cell
        }
    }
}
