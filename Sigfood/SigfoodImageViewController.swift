//
//  SigfoodImageView.swift
//  Sigfood
//
//  Created by Kett, Oliver on 30.11.15.
//  Copyright © 2015 Kett, Oliver. All rights reserved.
//

import UIKit
import JGProgressHUD

class SigfoodImageViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var imageID: Int = 0
    // initialize loading animation bevore first use
    var loadingAnimation = JGProgressHUD(style: JGProgressHUDStyle.dark)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // SwiftSpinner.show()
        loadingAnimation?.show(in: self.view)
        OperationQueue().addOperation {
            guard let data = try? Data(contentsOf: URL(string: "https://www.sigfood.de/?do=getimage&bildid=\(self.imageID)&width=800")!) else { self.hideView(nil); return }
            guard let image = UIImage(data: data) else { self.hideView(nil); return }
            OperationQueue.main.addOperation {
                let blurEffect = UIBlurEffect(style: .extraLight)
                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                blurEffectView.frame = self.view.frame
                self.view.insertSubview(blurEffectView, at: 0)
                self.view.backgroundColor = UIColor(patternImage: image)
                self.imageView.contentMode = .scaleAspectFit
                self.imageView.image = image
                // SwiftSpinner.hide()
                self.loadingAnimation?.dismiss(animated: false)
            }
        }
    }
    
    @IBAction func hideView(_ sender: AnyObject?) {
        // SwiftSpinner.hide()
        self.loadingAnimation?.dismiss(animated: false)
        presentingViewController?.dismiss(animated: true, completion: nil)
    }

}
